package com.tugas1.tugaswhatsapp

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class PageAdapter(fsa: FragmentManager): FragmentPagerAdapter(fsa)  {
    private val pages = listOf(CallFragment(), ChatFragment(), ContactFragment() )

     override fun getItem(position: Int): Fragment {
         return pages[position]
     }

     override fun getCount(): Int {
         return pages.size
     }

     override fun getPageTitle(position: Int): CharSequence? {
         return when(position){
             0 -> "Call"
             1 -> "Chat"
             else -> "Contact"

         }
     }
 }